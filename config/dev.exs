import Config

config :compiler_toolkit,
  base_recognition: CompilerToolkit.Frontend.Recognition,
  base_streaming: CompilerToolkit.Streaming,
  logician_recognition: CompilerToolkit.Language.Logician.Recognition