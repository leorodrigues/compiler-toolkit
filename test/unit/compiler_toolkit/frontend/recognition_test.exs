defmodule CompilerToolkit.Frontend.RecognitionTest do
  use ExUnit.Case

  alias CompilerToolkit.Frontend.Recognition

  @alphabet '\'"_0123456789'
    ++ 'abcdefghijklmnopqrstuvxyzw'
    ++ 'ABCDEFGHIJKLMNOPQRSTUVXYZW'
    ++ '[]{}.:|<>;/\\~-=+()!@#$%&*'

  @id_letter '_0123456789'
    ++ 'abcdefghijklmnopqrstuvxyzw'
    ++ 'ABCDEFGHIJKLMNOPQRSTUVXYZW'

  test "expand_transitions/2 should transform specs into a map" do
    expected_result = %{
      { 1, 5 } => "first_output",
      { 1, 6 } => "first_output",
      { 1, 7 } => "first_output",
      { 2, 8 } => "second_output"
    }

    transition_specifications = [
      { { 1, :a }, { "first_output", :follow } },
      { { 2, :b }, { "second_output", :accept, :test } }
    ]

    assert ^expected_result = transition_specifications
      |> Recognition.expand_transitions(fn
             :a -> [5,6,7]
             :b -> [8]
           end)
  end

  test "expand_classes/1 should expand :any" do
    assert are_similar_lists(Recognition.expand_classes(:any), ' ' ++ @alphabet)
  end

  test "expand_classes/1 should expand :left_bracket" do
    assert '[' = Recognition.expand_classes(:left_bracket)
  end

  test "expand_classes/1 should expand :right_bracket" do
    assert ']' = Recognition.expand_classes(:right_bracket)
  end

  test "expand_classes/1 should expand :id_letter" do
    assert are_similar_lists(Recognition.expand_classes(:id_letter), @id_letter)
  end

  test "expand_classes/1 should expand :digit" do
    assert are_similar_lists(Recognition.expand_classes(:digit), '0123456789')
  end

  test "expand_classes/1 should expand :dot" do
    assert '.' = Recognition.expand_classes(:dot)
  end

  test "expand_classes/1 should expand :double_quote" do
    assert '"' = Recognition.expand_classes(:double_quote)
  end

  test "expand_classes/1 should expand :single_quote" do
    assert '\'' = Recognition.expand_classes(:single_quote)
  end

  test "expand_classes/1 should expand :back_slash" do
    assert '\\' = Recognition.expand_classes(:back_slash)
  end

  test "expand_classes/1 should expand :not_double_quote" do
    assert are_similar_lists(
             Recognition.expand_classes(:double_quote_content),
             alphabet_except('"\\'))
  end

  test "expand_classes/1 should expand :not_single_quote" do
    assert are_similar_lists(
             Recognition.expand_classes(:single_quote_content),
             alphabet_except('\'\\'))
  end

  test "expand_classes/1 should expand an arbitrary codepoint" do
    assert [999] = Recognition.expand_classes(999)
  end

  defp are_similar_lists(a, b), do: Enum.frequencies(a) == Enum.frequencies(b)

  defp alphabet_except(l), do: Enum.filter(@alphabet, &(&1 not in l)) ++ ' '
end