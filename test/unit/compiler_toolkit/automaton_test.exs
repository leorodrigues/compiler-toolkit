defmodule CompilerToolkit.AutomatonTest do
  use ExUnit.Case

  alias CompilerToolkit.Automaton.Classes
  alias CompilerToolkit.Automaton

  @lower_case_simple_letters 0x61..0x7A
  @lower_case_latin_letters1 0xE0..0xF6
  @lower_case_latin_letters2 0xF8..0xFF

  @upper_case_simple_letters 0x41..0x5A
  @upper_case_latin_letters1 0xC0..0xD6
  @upper_case_latin_letters2 0xD8..0xDE

  @complete_codepoint_range 0x00..0xFF

  @decimal_digits 0x30..0x39

  @symbols_codepoint_variations %{
    0x0B => Classes.tab,
    0x20 => Classes.space,
    0x22 => Classes.double_quote,
    0x23 => Classes.sharp_sign,
    0x24 => Classes.dollar_sign,
    0x25 => Classes.percent_sign,
    0x26 => Classes.ampersand,
    0x27 => Classes.single_quote,
    0x2a => Classes.asterisk,
    0x2b => Classes.plus_sign,
    0x2c => Classes.comma,
    0x2d => Classes.minus_sign,
    0x2e => Classes.dot,
    0x2f => Classes.forward_slash,
    0x3a => Classes.colon,
    0x3b => Classes.semi_colon,
    0x3c => Classes.lesser_sign,
    0x3d => Classes.equal_sign,
    0x3e => Classes.greater_sign,
    0x3f => Classes.question_mark,
    0x40 => Classes.at_sign,
    0x5b => Classes.left_bracket,
    0x5c => Classes.back_slash,
    0x5d => Classes.right_bracket,
    0x5e => Classes.circumflex,
    0x5f => Classes.underline,
    0x7c => Classes.pipe,
    0x7e => Classes.tilde
  }

  @alphabet Enum.flat_map([
    Map.keys(@symbols_codepoint_variations),
    @lower_case_simple_letters,
    @lower_case_latin_letters1,
    @lower_case_latin_letters2,
    @upper_case_simple_letters,
    @upper_case_latin_letters1,
    @upper_case_latin_letters2,
    @decimal_digits
  ], &(&1))

  test "classify_symbol/1 should identify lower case letters" do
    for value <- @lower_case_simple_letters do
      assert Classes.lower_case_letter == Automaton.classify_symbol(value)
    end
  end

  test "classify_symbol/1 should identify special lower case letters" do
    for value <- @lower_case_latin_letters1 do
      assert Classes.lower_case_letter == Automaton.classify_symbol(value)
    end

    for value <- @lower_case_latin_letters2 do
      assert Classes.lower_case_letter == Automaton.classify_symbol(value)
    end
  end

  test "classify_symbol/1 should identify upper case letters" do
    for value <- @upper_case_simple_letters do
      assert Classes.upper_case_letter == Automaton.classify_symbol(value)
    end
  end

  test "classify_symbol/1 should identify special upper case letters" do
    for value <- @upper_case_latin_letters1 do
      assert Classes.upper_case_letter == Automaton.classify_symbol(value)
    end

    for value <- @upper_case_latin_letters2 do
      assert Classes.upper_case_letter == Automaton.classify_symbol(value)
    end
  end

  test "classify_symbol/1 should identify digits" do
    for value <- @decimal_digits do
      assert Classes.decimal_digit == Automaton.classify_symbol(value)
    end
  end

  test "classify_symbol/1 should identify specific characters" do
    for {codepoint, classification} <- @symbols_codepoint_variations do
      assert ^classification = Automaton.classify_symbol(codepoint)
    end
  end

  test "classify_symbol/1 should fail at every codepoint outside of alphabet" do
    in_alphabet = fn codepoint -> codepoint in @alphabet end
    outside_alphabet = @complete_codepoint_range |> Stream.reject(in_alphabet)

    for c <- outside_alphabet do
      assert Classes.unknown == Automaton.classify_symbol(c)
    end
  end
end
