defmodule CompilerToolkit.Language.Logician.RecognitionTest do
  use CompilerToolkit.Mockery.MockCase

  import ExDoubles

  alias CompilerToolkit.Language.Logician.Recognition
  alias CompilerToolkit.Mockery.RecognitionMock

  @id_letter '_abcdefghijklmnopqrstuvxyzwABCDEFGHIJKLMNOPQRSTUVXYZW0123456789'

  setup do
    RecognitionMock.reset()
    :ok
  end

  test "expand_classes/1 should delegate to the base recognition logic" do
    { :ok, mock_fn } = mock(:expand_classes, 1)
    RecognitionMock.replace_expand_classes(mock_fn)
    when_called(:expand_classes, 'imaginary_list')

    assert 'imaginary_list' = Recognition.expand_classes(:imaginary_class)

    verify(:expand_classes, once())
    verify(:expand_classes, called_with([:imaginary_class]))
  end

  test "expand_classes/1 should expand :id_head" do
    id_head = '_bcdefghjklmpqrstuvxyzwABCDEFGHIJKLMNOPQRSTUVXYZW'
    assert are_similar_lists(Recognition.expand_classes(:id_head), id_head)
  end

  test "expand_classes/1 should expand :id_letter_minus_d" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_d),
      id_letter_except('d'))
  end

  test "expand_classes/1 should expand :id_letter_minus_e" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_e),
      id_letter_except('e'))
  end

  test "expand_classes/1 should expand :id_letter_minus_i" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_i),
      id_letter_except('i'))
  end

  test "expand_classes/1 should expand :id_letter_minus_l" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_l),
      id_letter_except('l'))
  end

  test "expand_classes/1 should expand :id_letter_minus_m" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_m),
      id_letter_except('m'))
  end

  test "expand_classes/1 should expand :id_letter_minus_n" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_n),
      id_letter_except('n'))
  end

  test "expand_classes/1 should expand :id_letter_minus_o" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_o),
      id_letter_except('o'))
  end

  test "expand_classes/1 should expand :id_letter_minus_p" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_p),
      id_letter_except('p'))
  end

  test "expand_classes/1 should expand :id_letter_minus_r" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_r),
      id_letter_except('r'))
  end

  test "expand_classes/1 should expand :id_letter_minus_s" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_s),
      id_letter_except('s'))
  end

  test "expand_classes/1 should expand :id_letter_minus_t" do
    assert are_similar_lists(
      Recognition.expand_classes(:id_letter_minus_t),
      id_letter_except('t'))
  end

  defp are_similar_lists(a, b), do: Enum.frequencies(a) == Enum.frequencies(b)

  defp id_letter_except(l), do: Enum.filter(@id_letter, &(&1 not in l))
end