defmodule CompilerToolkit.Language.Logician.StreamTest do
  use CompilerToolkit.Mockery.MockCase

  alias CompilerToolkit.Mockery.RecognitionMock

  alias CompilerToolkit.Language.Logician.Streaming
  alias CompilerToolkit.Token
  alias CompilerToolkit.Line

  test "stream_tokens/1 should tokenize more than one line", ctx do
    expected_tokens = [
      make_token('super', 0, 5, :identifier, 1),
      make_token('duper', 0, 5, :identifier, 2)
    ]
    assert ^expected_tokens = tokenize "super\nduper", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize two numbers separated by space", ctx do
    expected_tokens = [
      make_token('42 1984', 0, 2, :number),
      make_token('42 1984', 3, 4, :number)
    ]
    assert ^expected_tokens = tokenize "42 1984", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize two identifiers separated by space", ctx do
    expected_tokens = [
      make_token('first second', 0, 5, :identifier),
      make_token('first second', 6, 6, :identifier)
    ]
    assert ^expected_tokens = tokenize "first second", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should treat chars outside of alphabet as end of token", ctx do
    expected_tokens = [
      make_token('firstÁsecond', 0, 5, :identifier),
      make_token('firstÁsecond', 6, 6, :identifier)
    ]
    assert ^expected_tokens = tokenize "firstÁsecond", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should throw if a double quote string stops halfway", ctx do
    input_string = "\"There is no place like"
    expected_error = { :error, "Broken token at [1, 22]." }
    assert ^expected_error = catch_throw(tokenize(input_string, ctx[:stream_tokens]))
  end

  test "stream_tokens/1 should tokenize \"mashed potatoes\" as a double quote string", ctx do
    expected_token = [make_token('"mashed potatoes"', 0, 17, :double_quote_string)]
    assert ^expected_token = tokenize "\"mashed potatoes\"", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize \"refrigerator\" as a double quote string", ctx do
    expected_token = [make_token('"refrigerator"', 0, 14, :double_quote_string)]
    assert ^expected_token = tokenize "\"refrigerator\"", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize \"\" as a double quote string", ctx do
    expected_token = [make_token('""', 0, 2, :double_quote_string)]
    assert ^expected_token = tokenize "\"\"", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should throw if a single quote string stops halfway", ctx do
    input_string = "'The most important thing you should know is"
    expected_error = { :error, "Broken token at [1, 43]." }
    assert ^expected_error = catch_throw(tokenize(input_string, ctx[:stream_tokens]))
  end

  test "stream_tokens/1 should tokenize 'smart beans' as a single quote string", ctx do
    expected_token = [make_token('\'smart beans\'', 0, 13, :single_quote_string)]
    assert ^expected_token = tokenize "'smart beans'", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'mastermind' as a single quote string", ctx do
    expected_token = [make_token('\'mastermind\'', 0, 12, :single_quote_string)]
    assert ^expected_token = tokenize "'mastermind'", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize '' as a single quote string", ctx do
    expected_token = [make_token('\'\'', 0, 2, :single_quote_string)]
    assert ^expected_token = tokenize "''", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize '[' as the beginning of a list", ctx do
    expected_token = [make_token('[', 0, 1, :begin_list)]
    assert ^expected_token = tokenize "[", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize ']' as the end of a list", ctx do
    expected_token = [make_token(']', 0, 1, :end_list)]
    assert ^expected_token = tokenize "]", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should throw if a number stops halfway", ctx do
    expected_error = { :error, "Broken token at [1, 2]." }
    assert ^expected_error = catch_throw(tokenize("13.", ctx[:stream_tokens]))
  end

  test "stream_tokens/1 should tokenize '1' as a number", ctx do
    expected_token = [make_token('1', 0, 1, :number)]
    assert ^expected_token = tokenize "1", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize '17' as a number", ctx do
    expected_token = [make_token('17', 0, 2, :number)]
    assert ^expected_token = tokenize "17", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize '17.2' as a number", ctx do
    expected_token = [make_token('17.2', 0, 4, :number)]
    assert ^expected_token = tokenize "17.2", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should throw while trying to tokenize '11.'", ctx do
    expected_error = { :error, "Broken token at [1, 2]." }
    assert ^expected_error = catch_throw(tokenize("11.", ctx[:stream_tokens]))
  end

  test "stream_tokens/1 should tokenize 'a' as an identifier", ctx do
    expected_token = [make_token('a', 0, 1, :identifier)]
    assert ^expected_token = tokenize "a", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'an' as an identifier", ctx do
    expected_token = [make_token('an', 0, 2, :identifier)]
    assert ^expected_token = tokenize "an", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'andy' as an identifier", ctx do
    expected_token = [make_token('andy', 0, 4, :identifier)]
    assert ^expected_token = tokenize "andy", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize the keyword 'and'", ctx do
    expected_token = [make_token('and', 0, 3, :keyword_and)]
    assert ^expected_token = tokenize "and", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'o' as an identifier", ctx do
    expected_token = [make_token('o', 0, 1, :identifier)]
    assert ^expected_token = tokenize "o", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'org' as an identifier", ctx do
    expected_token = [make_token('org', 0, 3, :identifier)]
    assert ^expected_token = tokenize "org", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize the keyword 'or'", ctx do
    expected_token = [make_token('or', 0, 2, :keyword_or)]
    assert ^expected_token = tokenize "or", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'n' as an identifier", ctx do
    expected_token = [make_token('n', 0, 1, :identifier)]
    assert ^expected_token = tokenize "n", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'no' as an identifier", ctx do
    expected_token = [make_token('no', 0, 2, :identifier)]
    assert ^expected_token = tokenize "no", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'note' as an identifier", ctx do
    expected_token = [make_token('note', 0, 4, :identifier)]
    assert ^expected_token = tokenize "note", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize the keyword 'not'", ctx do
    expected_token = [make_token('not', 0, 3, :keyword_not)]
    assert ^expected_token = tokenize "not", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'i' as an identifier", ctx do
    expected_token = [make_token('i', 0, 1, :identifier)]
    assert ^expected_token = tokenize "i", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'im' as an identifier", ctx do
    expected_token = [make_token('im', 0, 2, :identifier)]
    assert ^expected_token = tokenize "im", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'imp' as an identifier", ctx do
    expected_token = [make_token('imp', 0, 3, :identifier)]
    assert ^expected_token = tokenize "imp", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'impl' as an identifier", ctx do
    expected_token = [make_token('impl', 0, 4, :identifier)]
    assert ^expected_token = tokenize "impl", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'impli' as an identifier", ctx do
    expected_token = [make_token('impli', 0, 5, :identifier)]
    assert ^expected_token = tokenize "impli", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'implie' as an identifier", ctx do
    expected_token = [make_token('implie', 0, 6, :identifier)]
    assert ^expected_token = tokenize "implie", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize 'impliess' as an identifier", ctx do
    expected_token = [make_token('impliess', 0, 8, :identifier)]
    assert ^expected_token = tokenize "impliess", ctx[:stream_tokens]
  end

  test "stream_tokens/1 should tokenize the keyword 'implies'", ctx do
    expected_token = [make_token('implies', 0, 7, :keyword_implies)]
    assert ^expected_token = tokenize "implies", ctx[:stream_tokens]
  end

  setup_all do
    RecognitionMock.reset()
    stream_tokens = Streaming.prepare_stream_tokens
    { :ok, [stream_tokens: stream_tokens] }
  end

  setup do
    RecognitionMock.reset()
    :ok
  end

  defp make_token(line_content, column, length, type) do
    make_token(line_content, column, length, type, 1)
  end

  defp make_token(line_content, column, length, type, line_number) do
    %Token{
      line: %Line{ content: line_content, number: line_number },
      column: column,
      length: length,
      type: type
    }
  end

  defp tokenize(input, stream_tokens) do
    input
      |> Stream.unfold(&String.next_codepoint/1)
      |> stream_tokens.()
      |> Enum.to_list
  end
end