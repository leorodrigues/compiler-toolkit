defmodule CompilerToolkit.StreamingTest do
  use ExUnit.Case

  alias CompilerToolkit.StreamingMock

  alias CompilerToolkit.Line
  alias CompilerToolkit.Token
  alias CompilerToolkit.Streaming

  test "stream_tokens/2 should fail with broken token error" do
    expected_error = { :error, "Broken token at [1, 4]." }

    pipeline = "frig"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
        &StreamingMock.accept_eleven_char_word/2,
        &StreamingMock.finish_token_with_nil/1)

    assert ^expected_error = catch_throw(Stream.run(pipeline))
  end

  test "stream_tokens/2 should fail with unknown token error" do
    expected_error = {
      :error, "Unknown token at [1,0]. Unknown char at column 9."
    }

    pipeline = "some-thin$"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
        &StreamingMock.accept_entire_string/2,
        &StreamingMock.finish_failing_at_the_end/1)

    assert ^expected_error = catch_throw(Stream.run(pipeline))
  end

  test "stream_tokens/2 should skip the N empty lines in between" do
    line_one = %Line{ number: 1, content: 'B' }
    line_three = %Line{ number: 3, content: 'A' }

    expected_result = [
      %Token{ column: 0, length: 1, type: :word, line: line_one },
      %Token{ column: 0, length: 1, type: :word, line: line_three },
    ]

    assert ^expected_result = "B\n\nA"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
           &StreamingMock.accept_simple_words/2,
           &StreamingMock.finish_accept_single_word/1)
      |> Enum.to_list
  end

  test "stream_tokens/2 should skip the first N empty lines" do
    expected_line = %Line{ number: 4, content: 'A' }
    expected_result = [
      %Token{ column: 0, length: 1, type: :word, line: expected_line }
    ]

    assert ^expected_result = "\n\n\nA"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
           &StreamingMock.accept_simple_words/2,
           &StreamingMock.finish_accept_single_word/1)
      |> Enum.to_list
  end

  test "stream_tokens/2 should skip the first empty line" do
    expected_line = %Line{ number: 2, content: 'A' }
    expected_result = [
      %Token{ column: 0, length: 1, type: :word, line: expected_line }
    ]

    assert ^expected_result = "\nA"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
           &StreamingMock.accept_simple_words/2,
           &StreamingMock.finish_accept_single_word/1)
      |> Enum.to_list
  end

  test "stream_tokens/2 should accept a single char token" do
    expected_line = %Line{ number: 1, content: 'A' }
    expected_result = [
      %Token{ column: 0, length: 1, type: :word, line: expected_line }
    ]

    assert ^expected_result = "A"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
           &StreamingMock.accept_simple_words/2,
           &StreamingMock.finish_accept_single_word/1)
      |> Enum.to_list
  end

  test "stream_tokens/2 should accept a two line string" do
    expected_line_one = %Line{ number: 1, content: 'hello' }
    expected_line_two = %Line{ number: 2, content: 'world' }
    expected_result = [
      %Token{ column: 0, length: 5, type: :word, line: expected_line_one },
      %Token{ column: 0, length: 5, type: :word, line: expected_line_two }
    ]

    assert ^expected_result = "hello\nworld"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
        &StreamingMock.accept_simple_words/2,
        &StreamingMock.finish_accept_single_word/1)
      |> Enum.to_list
  end

  test "stream_tokens/2 should emit a single token" do
    expected_line = %Line{ number: 1, content: 'refrigerator' }
    expected_result = [
      %Token{ column: 0, length: 12, type: :identifier, line: expected_line }
    ]

    assert ^expected_result = "refrigerator"
      |> Stream.unfold(&String.next_codepoint/1)
      |> Streaming.stream_tokens(
           &StreamingMock.accept_eleven_char_word/2,
           &StreamingMock.finish_token_with_nil/1)
      |> Enum.to_list
  end
end
