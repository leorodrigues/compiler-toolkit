defmodule CompilerToolkit.Mockery.MockCase do
  use ExUnit.CaseTemplate

  alias CompilerToolkit.Mockery.Stubbing

  setup_all do
    { :ok, pid } = start_supervised(Stubbing)
    { :ok, [ stubbing_pid: pid ] }
  end
end