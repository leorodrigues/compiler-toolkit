defmodule CompilerToolkit.Mockery.RecognitionMock do
  alias CompilerToolkit.Frontend.Recognition
  alias CompilerToolkit.Mockery.Stubbing

  def id_letter() do
    Recognition.id_letter
  end

  def expand_transitions(transition_specs, expand_codepoint_class) do
    fun = Stubbing.get({ :base_recognition, :expand_transitions })
    fun.(transition_specs, expand_codepoint_class)
  end

  def collect_acceptance_states(transition_specs) do
    fun = Stubbing.get({ :base_recognition, :collect_acceptance_states })
    fun.(transition_specs)
  end

  def expand_classes(class) do
    fun = Stubbing.get({ :base_recognition, :expand_classes })
    fun.(class)
  end

  def replace_expand_classes(implementation) do
    Stubbing.set({ :base_recognition, :expand_classes }, implementation)
    implementation
  end

  def reset do
    Stubbing.set(
      { :base_recognition, :expand_classes },
      &Recognition.expand_classes/1)

    Stubbing.set(
      { :base_recognition, :expand_transitions },
      &Recognition.expand_transitions/2)

    Stubbing.set(
      { :base_recognition, :collect_acceptance_states },
      &Recognition.collect_acceptance_states/1)
  end
end
