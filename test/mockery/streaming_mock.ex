defmodule CompilerToolkit.StreamingMock do
  def accept_simple_words(indexed_codepoint, state) do
    case { indexed_codepoint, state } do
      { { _, _, 0x20 }, nil } ->
        { nil, { indexed_codepoint, 1 } }

      { { _, _, _ }, nil } ->
        { nil, { indexed_codepoint, 2 } }

      { { _, _, 0x20 }, { { l, c, 0x20 }, 1 } } ->
        { { :keep, l, c }, { indexed_codepoint, 1 } }

      { { _, _, 0x20 }, { { l, c, _ }, 2 } } ->
        { { :accept, :word, l, c }, { indexed_codepoint, 1 } }

      { { _, _, _ }, { { l, c, 0x20 }, 1 } } ->
        { { :accept, :space, l, c }, { indexed_codepoint, 2 } }

      { _, { { l, c, _ }, 2 } } ->
        { { :keep, l, c }, { indexed_codepoint, 2 } }
    end
  end

  def finish_accept_single_word(nil), do: nil
  def finish_accept_single_word({ { line, column, _ }, state }) do
    case state do
      1 -> { { :accept, :space, line, column }, nil }
      2 -> { { :accept, :word, line, column }, nil }
    end
  end

  def accept_eleven_char_word(indexed_codepoint, state) do
    case { indexed_codepoint, state } do
      { { line, column, _ }, nil } ->
        { { :keep, line, column }, 1 }

      { { line, column, _ }, 11 } ->
        { { :accept, :identifier, line, column }, nil }

      { { line, column, _ }, count } ->
        { { :keep, line, column }, count + 1 }
    end
  end

  def accept_entire_string(indexed_codepoint, state) do
    case { indexed_codepoint, state } do
      { _, nil } -> { nil, indexed_codepoint }

      { _, { line, column, _ } } ->
        { { :keep, line, column }, indexed_codepoint }
    end
  end

  def finish_failing_at_the_end({ line, column, _ }) do
    { { :unknown, line, column }, nil }
  end

  def finish_token_with_nil(_), do: nil
end