# CompilerToolkit

A set of tools used to build program compilers. From token and grammar
definitions to intermediary code builders, symbol tables and instruction set
emitters.

## Installation

The package can be installed by adding `compiler_toolkit` to your list of
dependencies in `mix.exs` (unfortunately it is not available on Hex):

```elixir
def deps do
  [
    {:compiler_toolkit, git: "git://bitbucket.org/leorodrigues/compiler-toolkit.git"}
  ]
end
```

