defmodule CompilerToolkit.MixProject do
  use Mix.Project

  def project do
    [
      app: :compiler_toolkit,
      version: "0.0.1-alpha",
      elixir: "~> 1.11",
      elixirc_paths: elixirc_paths(Mix.env),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [ :logger ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      { :dialyxir, "~> 0.5", only: [:dev], runtime: false },
      { :exdoubles, "~> 0.2.1", only: [:test] },
      { :excoveralls, "~> 0.10", only: [:test] }
    ]
  end

  defp elixirc_paths(env_atom) do
    case env_atom do
      :test -> ["lib", "test/mockery"]
      _ -> ["lib"]
    end
  end
end
