defmodule CompilerToolkit.Language.Logician.Recognition do

  @base_recognition Application.get_env(:compiler_toolkit, :base_recognition)

  @id_letter @base_recognition.id_letter

  def transition_specification do
    [
      # Recognition of list boundaries
      { { nil, :left_bracket }, { 0x02, :accept, :begin_list } },
      { { nil, :right_bracket }, { 0x03, :accept, :end_list } },

      # Recognition of numbers
      { { nil, :digit }, { 0x04, :accept, :number } },
      { { 0x04, :digit }, { 0x05, :accept, :number } },
      { { 0x04, :dot }, { 0x06, :follow } },
      { { 0x05, :digit }, { 0x05, :accept, :number } },
      { { 0x05, :dot }, { 0x06, :follow } },
      { { 0x06, :digit }, { 0x07, :accept, :number } },
      { { 0x07, :digit }, { 0x07, :accept, :number } },

      # Recognition of double quoted strings
      { { nil, :double_quote }, { 0x08, :follow } },
      { { 0x08, :double_quote }, { 0x0b, :accept, :double_quote_string } },
      { { 0x08, :double_quote_content }, { 0x09, :follow } },
      { { 0x08, :back_slash }, { 0x0a, :follow } },
      { { 0x09, :double_quote_content }, { 0x09, :follow } },
      { { 0x09, :back_slash }, { 0x0a, :follow } },
      { { 0x09, :double_quote }, { 0x0b, :accept, :double_quote_string } },
      { { 0x0a, :any }, { 0x08, :follow } },

      # Recognition of single quoted strings
      { { nil, :single_quote }, { 0x0c, :follow } },
      { { 0x0c, :single_quote }, { 0x0f, :accept, :single_quote_string } },
      { { 0x0c, :single_quote_content }, { 0x0d, :follow } },
      { { 0x0c, :back_slash }, { 0x0e, :follow } },
      { { 0x0d, :single_quote_content }, { 0x0d, :follow } },
      { { 0x0d, :back_slash }, { 0x0e, :follow } },
      { { 0x0d, :single_quote }, { 0x0f, :accept, :single_quote_string } },
      { { 0x0e, :any }, { 0x0c, :follow } },

      # Recognition of identifiers
      { { nil, :id_head }, { 0x10, :accept, :identifier } },
      { { 0x10, :id_letter }, { 0x11, :accept, :identifier } },
      { { 0x11, :id_letter }, { 0x11, :accept, :identifier } },

      # Recognition of keyword "implies"
      { { nil, 0x69 }, { 0x12, :accept, :identifier } },
      { { 0x12, 0x6d }, { 0x13, :accept, :identifier } },
      { { 0x13, 0x70 }, { 0x14, :accept, :identifier } },
      { { 0x14, 0x6c }, { 0x15, :accept, :identifier } },
      { { 0x15, 0x69 }, { 0x16, :accept, :identifier } },
      { { 0x16, 0x65 }, { 0x17, :accept, :identifier } },
      { { 0x17, 0x73 }, { 0x18, :accept, :keyword_implies } },

      { { 0x12, :id_letter_minus_m }, { 0x10, :accept, :identifier } },
      { { 0x13, :id_letter_minus_p }, { 0x10, :accept, :identifier } },
      { { 0x14, :id_letter_minus_l }, { 0x10, :accept, :identifier } },
      { { 0x15, :id_letter_minus_i }, { 0x10, :accept, :identifier } },
      { { 0x16, :id_letter_minus_e }, { 0x10, :accept, :identifier } },
      { { 0x17, :id_letter_minus_s }, { 0x10, :accept, :identifier } },
      { { 0x18, :id_letter }, { 0x10, :accept, :identifier } },

      # Recognition of keyword "and"
      { { nil, 0x61 }, { 0x19, :accept, :identifier } },
      { { 0x19, 0x6e }, { 0x1a, :accept, :identifier } },
      { { 0x1a, 0x64 }, { 0x1b, :accept, :keyword_and } },

      { { 0x19, :id_letter_minus_n }, { 0x10, :accept, :identifier } },
      { { 0x1a, :id_letter_minus_d }, { 0x10, :accept, :identifier } },
      { { 0x1b, :id_letter }, { 0x10, :accept, :identifier } },

      # Recognition of keyword "not"
      { { nil, 0x6e }, { 0x1c, :accept, :identifier } },
      { { 0x1c, 0x6f }, { 0x1d, :accept, :identifier } },
      { { 0x1d, 0x74 }, { 0x1e, :accept, :keyword_not } },

      { { 0x1c, :id_letter_minus_o }, { 0x10, :accept, :identifier } },
      { { 0x1d, :id_letter_minus_t }, { 0x10, :accept, :identifier } },
      { { 0x1e, :id_letter }, { 0x10, :accept, :identifier } },

      # Recognition of keyword "or"
      { { nil, 0x6f }, { 0x1f, :accept, :identifier } },
      { { 0x1f, 0x72 }, { 0x20, :accept, :keyword_or } },

      { { 0x1f, :id_letter_minus_r }, { 0x10, :accept, :identifier } },
      { { 0x20, :id_letter }, { 0x10, :accept, :identifier } }
    ]
  end

  def expand_classes(class_name_or_codepoint) do
    case class_name_or_codepoint do
      :id_head -> Enum.filter(@id_letter, &(&1 not in 'aino0123456789'))
      :id_letter_minus_d -> Enum.filter(@id_letter, &(&1 not in 'd'))
      :id_letter_minus_e -> Enum.filter(@id_letter, &(&1 not in 'e'))
      :id_letter_minus_i -> Enum.filter(@id_letter, &(&1 not in 'i'))
      :id_letter_minus_l -> Enum.filter(@id_letter, &(&1 not in 'l'))
      :id_letter_minus_m -> Enum.filter(@id_letter, &(&1 not in 'm'))
      :id_letter_minus_n -> Enum.filter(@id_letter, &(&1 not in 'n'))
      :id_letter_minus_o -> Enum.filter(@id_letter, &(&1 not in 'o'))
      :id_letter_minus_p -> Enum.filter(@id_letter, &(&1 not in 'p'))
      :id_letter_minus_r -> Enum.filter(@id_letter, &(&1 not in 'r'))
      :id_letter_minus_s -> Enum.filter(@id_letter, &(&1 not in 's'))
      :id_letter_minus_t -> Enum.filter(@id_letter, &(&1 not in 't'))
      some_other_value -> @base_recognition.expand_classes(some_other_value)
    end
  end
end