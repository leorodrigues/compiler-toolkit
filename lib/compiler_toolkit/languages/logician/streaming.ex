defmodule CompilerToolkit.Language.Logician.Streaming do
  @base_streaming Application.get_env(
    :compiler_toolkit, :base_streaming)

  @base_recognition Application.get_env(
    :compiler_toolkit, :base_recognition)

  @logician_recognition Application.get_env(
    :compiler_toolkit, :logician_recognition)

  def prepare_stream_tokens do
    transition_specs = @logician_recognition.transition_specification

    acceptances = @base_recognition.collect_acceptance_states(transition_specs)
    transitions = @base_recognition.expand_transitions(
      transition_specs, &@logician_recognition.expand_classes/1)

    finish = compose_finish(acceptances)
    accept = compose_accept(transitions, finish)

    fn stream -> @base_streaming.stream_tokens(stream, accept, finish) end
  end

  defp compose_accept(transitions, finish) do
    fn input, state ->
      accept(input, transitions[key(input, state)], state, finish)
    end
  end

  defp compose_finish(acceptances) do
    fn state -> finish(state, acceptances) end
  end

  defp accept(_, nil, nil, _), do: nil

  defp accept(input, transition, nil, _), do: { nil, { transition, input } }

  defp accept(_, nil, state, finish), do: finish.(state)

  defp accept(input, transition, { _, { line, column, _ } }, _) do
    { { :keep, line, column }, { transition, input }}
  end

  defp finish(nil, _), do: nil

  defp finish({ transition, { line, column, _ } = input }, acceptances) do
    case acceptances[transition] do
      nil -> throw_broken_token(input)
      type -> { { :accept, type, line, column }, nil }
    end
  end

  defp key({ _, _, codepoint }, nil), do: { nil, codepoint }

  defp key({ _, _, codepoint }, { transition, _ }), do: { transition, codepoint }

  defp throw_broken_token({ line, column, _ }) do
    throw { :error, "Broken token at [#{line}, #{column}]." }
  end
end