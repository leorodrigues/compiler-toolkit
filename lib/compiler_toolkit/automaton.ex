defmodule CompilerToolkit.Automaton do

  defmodule Classes do
    @spec lower_case_letter :: :lower_case_letter
    def lower_case_letter, do: :lower_case_letter

    @spec upper_case_letter :: :upper_case_letter
    def upper_case_letter, do: :upper_case_letter

    @spec decimal_digit :: :decimal_digit
    def decimal_digit, do: :decimal_digit

    @spec tab :: :tab
    def tab, do: :tab

    @spec space :: :space
    def space, do: :space

    @spec double_quote :: :double_quote
    def double_quote, do: :double_quote

    @spec sharp_sign :: :sharp_sign
    def sharp_sign, do: :sharp_sign

    @spec dollar_sign :: :dollar_sign
    def dollar_sign, do: :dollar_sign

    @spec percent_sign :: :percent_sign
    def percent_sign, do: :percent_sign

    @spec ampersand :: :ampersand
    def ampersand, do: :ampersand

    @spec single_quote :: :single_quote
    def single_quote, do: :single_quote

    @spec asterisk :: :asterisk
    def asterisk, do: :asterisk

    @spec plus_sign :: :plus_sign
    def plus_sign, do: :plus_sign

    @spec comma :: :comma
    def comma, do: :comma

    @spec minus_sign :: :minus_sign
    def minus_sign, do: :minus_sign

    @spec dot :: :dot
    def dot, do: :dot

    @spec forward_slash :: :forward_slash
    def forward_slash, do: :forward_slash

    @spec colon :: :colon
    def colon, do: :colon

    @spec semi_colon :: :semi_colon
    def semi_colon, do: :semi_colon

    @spec lesser_sign :: :lesser_sign
    def lesser_sign, do: :lesser_sign

    @spec equal_sign :: :equal_sign
    def equal_sign, do: :equal_sign

    @spec greater_sign :: :greater_sign
    def greater_sign, do: :greater_sign

    @spec question_mark :: :question_mark
    def question_mark, do: :question_mark

    @spec at_sign :: :at_sign
    def at_sign, do: :at_sign

    @spec left_bracket :: :left_bracket
    def left_bracket, do: :left_bracket

    @spec back_slash :: :back_slash
    def back_slash, do: :back_slash

    @spec right_bracket :: :right_bracket
    def right_bracket, do: :right_bracket

    @spec circumflex :: :circumflex
    def circumflex, do: :circumflex

    @spec underline :: :underline
    def underline, do: :underline

    @spec pipe :: :pipe
    def pipe, do: :pipe

    @spec tilde :: :tilde
    def tilde, do: :tilde

    @spec unknown :: :unknown
    def unknown, do: :unknown
  end

  def classify_symbol(symbol_value) do
    case symbol_value do
      s when s in 0x61..0x7A -> Classes.lower_case_letter
      s when s in 0xE0..0xF6 -> Classes.lower_case_letter
      s when s in 0xF8..0xFF -> Classes.lower_case_letter
      s when s in 0x41..0x5A -> Classes.upper_case_letter
      s when s in 0xC0..0xD6 -> Classes.upper_case_letter
      s when s in 0xD8..0xDE -> Classes.upper_case_letter
      s when s in 0x30..0x39 -> Classes.decimal_digit
      0x0B -> Classes.tab
      0x20 -> Classes.space
      0x22 -> Classes.double_quote
      0x23 -> Classes.sharp_sign
      0x24 -> Classes.dollar_sign
      0x25 -> Classes.percent_sign
      0x26 -> Classes.ampersand
      0x27 -> Classes.single_quote
      0x2A -> Classes.asterisk
      0x2B -> Classes.plus_sign
      0x2C -> Classes.comma
      0x2D -> Classes.minus_sign
      0x2E -> Classes.dot
      0x2F -> Classes.forward_slash
      0x3A -> Classes.colon
      0x3B -> Classes.semi_colon
      0x3C -> Classes.lesser_sign
      0x3D -> Classes.equal_sign
      0x3E -> Classes.greater_sign
      0x3F -> Classes.question_mark
      0x40 -> Classes.at_sign
      0x5B -> Classes.left_bracket
      0x5C -> Classes.back_slash
      0x5D -> Classes.right_bracket
      0x5E -> Classes.circumflex
      0x5F -> Classes.underline
      0x7C -> Classes.pipe
      0x7E -> Classes.tilde
      _ -> Classes.unknown
    end
  end
end
