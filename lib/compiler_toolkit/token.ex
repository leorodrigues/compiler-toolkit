defmodule CompilerToolkit.Token do
  alias CompilerToolkit.Token
  alias CompilerToolkit.Line

  defstruct type: nil, line: nil, column: 0, length: 0

  @type t :: %Token{
    type: atom,
    line: Line.t,
    column: non_neg_integer,
    length: non_neg_integer
  }
end
