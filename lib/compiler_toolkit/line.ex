defmodule CompilerToolkit.Line do
  alias CompilerToolkit.Line

  defstruct number: 0, content: [ ]

  @type t :: %Line{
    number: non_neg_integer,
    content: charlist
  }
end
