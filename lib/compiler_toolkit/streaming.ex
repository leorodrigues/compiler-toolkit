defmodule CompilerToolkit.Streaming do

  alias CompilerToolkit.Line
  alias CompilerToolkit.Token
  alias CompilerToolkit.Automaton

  def stream_tokens(char_stream, accept, finish_accept) do
    char_stream
      |> Stream.map(&to_integer_representation/1)
      |> Stream.chunk_while(nil, &add_to_proto_line/2, &end_proto_line/1)
      |> Stream.transform(nil, append_tokens(accept, finish_accept))
      |> Stream.flat_map(&to_token_struct/1)
  end

  defp append_tokens(accept, finish_accept) do
    fn { line, content }, _ ->
      wrapped_accept = wrap_accept(accept)
      wrapped_finish = wrap_finish_accept(finish_accept)

      proto_tokens = content
        |> Stream.with_index
        |> Stream.map(add_line_number(line))
        |> Stream.chunk_while(nil, wrapped_accept, wrapped_finish)
        |> Stream.chunk_while(nil, &add_to_proto_token/2, &end_proto_token/1)
        |> Enum.to_list

      { [{ line, content, proto_tokens }], nil }
    end
  end

  defp add_line_number(line) do
    fn { codepoint, column } -> { line, column, codepoint } end
  end

  defp wrap_accept(accept) do
    fn icp, s -> envelope_accept_result(accept.(icp, s)) end
  end

  defp wrap_finish_accept(finish_accept) do
    fn s -> envelope_accept_result(finish_accept.(s)) end
  end

  defp envelope_accept_result(accepted_result) do
    case accepted_result do
      nil -> { :cont, nil }
      { nil, state } -> { :cont, state }
      { element, state } -> { :cont, element, state }
    end
  end

  defp add_to_proto_token(identified_codepoint, current_proto_token) do
    case { identified_codepoint, current_proto_token } do
      { { :keep, line, column }, nil } ->
        begin_token(line, column)

      { { :keep, _, _ }, { line, column, length } } ->
        continue_token(line, column, length + 1)

      { { :accept, type, _, column }, nil } ->
        accept_token(column, type)

      { { :accept, type, _, _ }, { _, column, length } } ->
        accept_token(column, length + 1, type)

      { { :unknown, _, _ }, _ } ->
        throw_unknown_token(current_proto_token)
    end
  end

  defp end_proto_token(current_proto_token) do
    case current_proto_token do
      nil -> { :cont, nil }
      _ -> throw_broken_token(current_proto_token)
    end
  end

  defp add_to_proto_line(codepoint, current_proto_line) do
    case { codepoint, current_proto_line } do
      { 0x0A, nil } -> { :cont, { 1, [ ] }, { 2, [ ] } }

      { _, nil } -> { :cont, { 1, [codepoint] } }

      { 0x0A, { count, content } } ->
        { :cont, { count, Enum.reverse(content) }, { count + 1, [ ] } }

      { _, { count, content } } ->
        { :cont, { count, [codepoint|content] } }
    end
  end

  defp end_proto_line({ count, content }) do
    { :cont, { count, Enum.reverse(content) }, nil }
  end

  defp to_token_struct({ line_number, line_content, tokens }) do
    line = %Line{ number: line_number, content: line_content }
    Enum.map(tokens, fn
      { c, l, t } -> %Token{ line: line, column: c, length: l, type: t }
    end)
  end

  defp begin_token(line, column) do
    { :cont, { line, column, 1 } }
  end

  defp continue_token(line, column, length) do
    { :cont, { line, column, length } }
  end

  defp accept_token(column, type) do
    { :cont, { column, 1, type }, nil }
  end

  defp accept_token(column, length, type) do
    { :cont, { column, length, type }, nil }
  end

  defp throw_unknown_token({ line, start, length }) do
    message = "Unknown token at [#{ line },#{ start }]. "
      <> "Unknown char at column #{ length }."
    throw { :error, message }
  end

  defp throw_broken_token({ line, start, length }) do
    message = "Broken token at [#{ line }, #{ start + length }]."
    throw { :error, message }
  end

  defp to_integer_representation(<<codepoint::utf8>>), do: codepoint
end
