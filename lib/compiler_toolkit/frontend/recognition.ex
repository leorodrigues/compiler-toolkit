defmodule CompilerToolkit.Frontend.Recognition do

  @alphabet '\'"_0123456789'
    ++ 'abcdefghijklmnopqrstuvxyzw'
    ++ 'ABCDEFGHIJKLMNOPQRSTUVXYZW'
    ++ '[]{}()<>.:;|/\\~-=+!@#$%&*'

  @id_letter '_0123456789'
    ++ 'abcdefghijklmnopqrstuvxyzw'
    ++ 'ABCDEFGHIJKLMNOPQRSTUVXYZW'

  def alphabet, do: @alphabet

  def id_letter, do: @id_letter

  def expand_classes(class) do
    case class do
      :left_bracket -> '['
      :right_bracket -> ']'
      :id_letter -> @id_letter
      :digit -> '0123456789'
      :dot -> '.'
      :double_quote -> '"'
      :single_quote -> '\''
      :back_slash -> '\\'
      :any -> @alphabet ++ ' '
      :double_quote_content -> list_except(@alphabet, '"\\') ++ ' '
      :single_quote_content -> list_except(@alphabet, '\'\\') ++ ' '
      codepoint -> [codepoint]
    end
  end

  def expand_transitions(transition_specs, expand_class) do
    transition_specs
      |> Enum.flat_map(compose_expand(expand_class))
      |> Enum.reduce(%{ }, &merge_transition/2)
  end

  def collect_acceptance_states(transition_specs) do
    transition_specs |> Enum.reduce(%{ }, &collect_accepts/2)
  end

  defp compose_expand(expand_class) do
    fn
      { { current_state, class }, { next_state, _, _ } } ->
        run_expansion(expand_class, class, current_state, next_state)

      { { current_state, class }, { next_state, _ } } ->
        run_expansion(expand_class, class, current_state, next_state)
    end
  end

  defp merge_transition({ key, next_state }, transitions) do
    Map.put(transitions, key, next_state)
  end

  defp run_expansion(expand_class, class, state, next_state) do
    expand_class.(class) |> Enum.map(to_key_value_tuple(state, next_state))
  end

  defp to_key_value_tuple(state, next_state) do
    fn codepoint -> { { state, codepoint }, next_state } end
  end

  defp list_except(given_list, exception_list) do
    Enum.filter(given_list, &(&1 not in exception_list))
  end

  defp collect_accepts({ _, {state, :accept, type} }, %{ } = acceptances) do
    Map.put(acceptances, state, type)
  end

  defp collect_accepts({ _, {_, :follow} }, %{ } = acceptances), do: acceptances
end
